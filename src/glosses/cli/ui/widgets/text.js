

export default class Text extends ateos.cli.ui.widget.Element {
    constructor(options = { }) {
        options.shrink = true;
        super(options);
    }
}
Text.prototype.type = "text";
