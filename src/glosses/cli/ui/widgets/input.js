

export default class Input extends ateos.cli.ui.widget.Element {
    constructor(options = { }) {
        super(options);
    }
}
Input.prototype.type = "input";
