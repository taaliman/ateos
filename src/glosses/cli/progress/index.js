const __ = ateos.lazify({
    Bar: "./bar",
    MultiBar: "./multi_bar",
    Presets: "./presets/index",
    format: () => ateos.lazify({
        Formatter: "./formatter",
        BarFormat: "./format_bar",
        ValueFormat: "./format_value",
        TimeFormat: "./format_time"
    }, null, require)
}, exports, require);

export default __;
