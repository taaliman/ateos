ateos.lazify({
    dirty: "./dirty",
    mock: "./mock",
    promise: "./promise",
    checkmark: "./checkmark",
    spy: "./spy",
    string: "./string"
}, ateos.asNamespace(exports), require);
