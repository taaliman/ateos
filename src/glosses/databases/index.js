ateos.lazify({
    level: "./level",
    mysql: "mysql2",
    orm: "sequelize",
    postgresql: "pg",
    sqlite3: "sqlite3"
}, ateos.asNamespace(exports), require);
