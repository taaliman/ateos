// Type definitions for ateos 0.6
// Project: https://gitlab.com/taaliman/ateos
// Definitions by: Taaliman <https://gitlab.com/taaliman>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.4

/// <reference path="./types/ateos.d.ts" />
/// <reference path="./types/glosses/app.d.ts" />
/// <reference path="./types/glosses/archives.d.ts" />
/// <reference path="./types/glosses/assertion.d.ts" />
/// <reference path="./types/glosses/collections/index.d.ts" />
/// <reference path="./types/glosses/compressors.d.ts" />
/// <reference path="./types/glosses/crypto/index.d.ts" />
/// <reference path="./types/glosses/data.d.ts" />
/// <reference path="./types/glosses/datetime.d.ts" />
/// <reference path="./types/glosses/events.d.ts" />
/// <reference path="./types/glosses/errors.d.ts" />
/// <reference path="./types/glosses/fake.d.ts" />
/// <reference path="./types/glosses/fast.d.ts" />
/// <reference path="./types/glosses/fs.d.ts" />
/// <reference path="./types/glosses/is.d.ts" />
/// <reference path="./types/glosses/math/index.d.ts" />
/// <reference path="./types/glosses/meta.d.ts" />
/// <reference path="./types/glosses/net/index.d.ts" />
/// <reference path="./types/glosses/pretty/index.d.ts" />
/// <reference path="./types/glosses/promise.d.ts" />
/// <reference path="./types/glosses/regex.d.ts" />
/// <reference path="./types/glosses/semver.d.ts" />
/// <reference path="./types/glosses/shani-global.d.ts" />
/// <reference path="./types/glosses/shani.d.ts" />
/// <reference path="./types/glosses/std.d.ts" />
/// <reference path="./types/glosses/streams.d.ts" />
/// <reference path="./types/glosses/system/index.d.ts" />
/// <reference path="./types/glosses/templating/index.d.ts" />
/// <reference path="./types/glosses/text/index.d.ts" />
/// <reference path="./types/glosses/utils.d.ts" />
/// <reference path="./types/glosses/vault.d.ts" />

declare const _ateos: typeof ateos;

export { _ateos as ateos };
